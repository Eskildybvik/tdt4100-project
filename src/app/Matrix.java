package app;



public class Matrix {
    private double[][] rows;
    private int numRows;
    private int numCols;

    /**
     * Parses a string to a new matrix
     * @param input String with matrix rows seperated by \n, and columns by a single whitespace
     * @return A new matrix parsed form the string
     * @throws IllegalArgumentException if the string is invalid.
     */
    public static Matrix parseMatrix(String input) throws IllegalArgumentException {
        String[] rowsStr = input.split("\n");
        int rowLength = rowsStr[0].split(" ").length;
        double[][] parsed = new double[rowsStr.length][rowLength];
        for (int row = 0; row < rowsStr.length; row++) {
            String[] unparsedRow = rowsStr[row].trim().split(" ");
            if (unparsedRow.length != rowLength)
                throw new IllegalArgumentException("All rows must have equal length");
            for (int col = 0; col < unparsedRow.length; col++) {
                parsed[row][col] = Double.parseDouble(unparsedRow[col]);
            }
        }
        return new Matrix(parsed);
    }

    /**
     * Creates a new matrix from a 2D double array
     * @param rows An array of arrays of doubles, where each array represents a row in the matrix
     * @throws IllegalArgumentException
     */
    public Matrix(double[][] rows) throws IllegalArgumentException {
        numRows = rows.length;
        if (numRows < 1) throw new IllegalArgumentException("A matrix must contain at least 1 row.");
        numCols = rows[0].length;
        if (numCols < 1) throw new IllegalArgumentException("A matrix must contain at least 1 column");

        for (double[] row : rows) {
            if (row.length != numCols) throw new IllegalArgumentException("All rows must be the same length");
        }

        this.rows = rows;
    }

    /**
     * Converts the matrix into a string representation
     * @return A string. Same format as parseMatrix
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (double[] row : this.rows) {
            for (double num : row) {
                // Don't add .0 at the end if results are integers
                if (num == (int)num) {
                    builder.append((int)num);
                }
                else {
                    builder.append(num);
                }
                builder.append(" ");
            }
            builder.deleteCharAt(builder.length()-1); // Removes last whitespace
            builder.append('\n');
        }
        return builder.toString();
    }

    public int getHeight() {
        return numRows;
    }

    public int getWidth() {
        return numCols;
    }

    public double getValue(int row, int col) {
        return this.rows[row][col];
    }

    public Vector getRow(int n) {
        return new Vector(rows[n]);
    }

    public Vector getCol(int n) {
        double[] col = new double[numRows];
        for (int i = 0; i < numRows; i++) {
            col[i] = this.getValue(i, n);
        }
        return new Vector(col);
    }

    /**
     * Performs matrix multiplication
     * @param by The matrix to multiply by (right side of multiplication sign)
     * @return A new matrix of the matrix product of this and by.
     * @throws IllegalArgumentException If this matrix' width isn't equal ty by's height
     */
    public Matrix multiply(Matrix by) throws IllegalArgumentException {
        if (this.getWidth() != by.getHeight())
            throw new IllegalArgumentException("Left matrix' width must be equal to right matrix' height.");

        double[][] temp = new double[this.getHeight()][by.getWidth()];
        for (int valRow = 0; valRow < temp.length; valRow++) {
            for (int valCol = 0; valCol < temp[0].length; valCol++) {
                // Matrix multiplication can be defined as dot products of rows and columns
                Vector thisRow = this.getRow(valRow);
                Vector byCol = by.getCol(valCol);
                temp[valRow][valCol] = thisRow.dotProduct(byCol);
            }
        }

        return new Matrix(temp);
    }
}
