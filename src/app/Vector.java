package app;

public class Vector {
    double[] values;

    /**
     * Creates a vector
     * @param values An array of doubles that will represent the vector
     */
    public Vector(double[] values) {
        this.values = values;
    }

    public double getValue(int n) {
        return values[n];
    }

    public int length() {
        return values.length;
    }

    /**
     * Calculates the dot product of two vectors
     * @param by What vector to multiply by
     * @return The dot product
     * @throws IllegalArgumentException If the vectors have different lengths
     */
    public double dotProduct(Vector by) throws IllegalArgumentException {
        if (this.length() != by.length())
            throw new IllegalArgumentException("Vectors must be the same length");
        double product = 0;
        for (int n = 0; n < this.length(); n++) {
            product += this.getValue(n)*by.getValue(n);
        }
        return product;
    }
}
