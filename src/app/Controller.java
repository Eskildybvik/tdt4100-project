package app;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;

public class Controller {
    @FXML
    Label lblStatus;
    @FXML
    TextArea txtLeftInput;
    @FXML
    TextArea txtRightInput;
    @FXML
    TextArea txtOutput;
    @FXML
    Button btnMultiply;
    @FXML
    Button btnImport;
    @FXML
    Button btnExport;
    @FXML
    Button btnLatex;

    public Stage primaryStage; // Needed for FileChooser

    FileRWImpl io;

    public void init(Stage primaryStage) {
        this.primaryStage = primaryStage;
        disableExportButtons();
        io = new FileRWImpl();
    }

    public void onMultiplyClick() {
        Matrix left;
        try {
            left = Matrix.parseMatrix(txtLeftInput.getText());
        }
        catch (IllegalArgumentException e) {
            lblStatus.setText("Error in left input: \n" + e.getMessage());
            return;
        }
        Matrix right;
        try {
            right = Matrix.parseMatrix(txtRightInput.getText());
        }
        catch (IllegalArgumentException e) {
            lblStatus.setText("Error in right input: \n" + e.getMessage());
            return;
        }

        try {
            Matrix product = left.multiply(right);
            txtOutput.setText(product.toString());
        }
        catch (IllegalArgumentException e) {
            lblStatus.setText("Error calculating: \n" + e.getMessage());
            return;
        }
        lblStatus.setText("Multiplication successful.");
        enableExportButtons();
    }

    public void onImportClick() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file to import");
        File chosen = fileChooser.showOpenDialog(primaryStage);
        if (chosen == null) return;
        String content;
        try {
            content = io.readFromFile(chosen.getPath(), true);
        }
        catch (FileNotFoundException e) {
            lblStatus.setText("Invalid file");
            return;
        }
        String leftString, rightString, outputString;
        try {
            // Reads left matrix. 5 is the length of the word "LEFT", plus the newline character.
            leftString = content.substring(content.indexOf("LEFT")+5, content.indexOf("RIGHT"));
            // Right matrix
            rightString = content.substring(content.indexOf("RIGHT")+6, content.indexOf("RESULT"));
            // Output matrix
            outputString = content.substring(content.indexOf("RESULT")+7);
        }
        catch (IndexOutOfBoundsException e) {
            lblStatus.setText("Invalid file");
            return;
        }

        Matrix newLeft, newRight, newResult;
        try {
            newLeft = Matrix.parseMatrix(leftString);
            newRight = Matrix.parseMatrix(rightString);
            newResult = Matrix.parseMatrix(outputString);
        }
        catch (IllegalArgumentException e) {
            lblStatus.setText("Invalid import file");
            return;
        }
        txtLeftInput.setText(newLeft.toString());
        txtRightInput.setText(newRight.toString());
        txtOutput.setText(newResult.toString());
        lblStatus.setText("Import successful");
        enableExportButtons();
    }

    public void onExportClick() {
        // The export buttons are only enabled when a successful multiplication is carried out, so we can assume
        // all inputs are valid
        StringBuilder content = new StringBuilder();
        content.append("LEFT\n");
        content.append(txtLeftInput.getText());
        content.append("\nRIGHT\n");
        content.append(txtRightInput.getText());
        content.append("\nRESULT\n");
        content.append(txtOutput.getText());
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose where to save file");
        File chosen = fileChooser.showSaveDialog(primaryStage);
        if (chosen == null) return;
        try {
            io.writeToFile(chosen.getPath(), content.toString());
        }
        catch (FileNotFoundException e) {
            lblStatus.setText("Invalid file");
            return;
        }
        lblStatus.setText("Export successful");
    }

    public void onLatexClick() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose where to save LaTeX file");
        fileChooser.setInitialFileName("multiplication.tex");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("TEX File", "*.tex"));
        File chosen = fileChooser.showSaveDialog(primaryStage);
        if (chosen == null) return;
        StringBuilder content = new StringBuilder();
        content.append("\\begin{math}\n\\begin{bmatrix}\n");
        content.append(txtLeftInput.getText().trim().replaceAll("\n", "\\\\\\\\").replaceAll(" ", "&"));
        content.append("\n\\end{bmatrix}\n\\cdot\n\\begin{bmatrix}\n");
        content.append(txtRightInput.getText().trim().replaceAll("\n", "\\\\\\\\").replaceAll(" ", "&"));
        content.append("\n\\end{bmatrix}\n=\n\\begin{bmatrix}\n");
        content.append(txtOutput.getText().trim().replaceAll("\n", "\\\\\\\\").replaceAll(" ", "&"));
        content.append("\n\\end{bmatrix}\n\\end{math}");
        try {
            io.writeToFile(chosen.getPath(), content.toString());
        }
        catch (FileNotFoundException e) {
            lblStatus.setText("Invalid file");
            return;
        }
        lblStatus.setText("Export successful");
    }

    public void disableExportButtons() {
        btnExport.setDisable(true);
        btnLatex.setDisable(true);
    }

    public void enableExportButtons() {
        btnExport.setDisable(false);
        btnLatex.setDisable(false);
    }
}
