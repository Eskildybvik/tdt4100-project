package app;

import java.io.FileNotFoundException;

public interface FileRW {
    public String readFromFile(String path) throws FileNotFoundException;
    public void writeToFile(String path, String content) throws FileNotFoundException;
}
