package app;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileRWImpl implements FileRW {
    @Override
    public String readFromFile(String path) throws FileNotFoundException {
        return readFromFile(path, false);
    }

    public String readFromFile(String path, boolean trimLines) throws FileNotFoundException {
        StringBuilder content = new StringBuilder();
        Scanner file = new Scanner(new FileReader(path));
        while (file.hasNextLine()) {
            if (trimLines) content.append(file.nextLine().trim());
            else content.append(file.nextLine());
            content.append('\n');
        }
        return content.toString();
    }

    @Override
    public void writeToFile(String path, String content) throws FileNotFoundException {
        PrintWriter file = new PrintWriter(path);
        file.print(content);
        file.close();
    }
}
